## Ciao, I'm Missy! 👋
> 🔔 This is my personal account, for my work handle check out [@missy-gitlab](https://gitlab.com/missy-gitlab)

💎 Backend Developer experienced with Ruby on Rails and Python

🦊 GitLab open source contributor since August 2022, [GitLab Hero](https://about.gitlab.com/community/heroes/members/#missy-davies) since June 2023, 2nd place winner of [July '23 Hackathon](https://forum.gitlab.com/t/announcing-the-fy24-q2-hackathon-results/91342)

💬 Recently on the Messaging team at Shopify building intelligence and automation features for [Shopify Inbox](https://www.shopify.com/inbox)
